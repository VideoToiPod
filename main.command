#!/usr/bin/env sh

# main.command
# VideoToiPod

#  Created by Pierre Andrews on 13/09/2007.
#  Copyright 2007 Pierre Andrews. All rights reserved.
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


if [[ ! "${overwrite}" ]]; then overwrite=0; fi
if [[ ! "${twopass}" ]]; then twopass=0; fi
case "${overwrite}" in
0)  over=  ;;
1)  over=" -y ";;
esac

while read line; do
	if [[ ! "${destdir}" ]]; then destdir=`dirname "$line"`; fi
	output=`basename "${line}"`;
	output=${output%\.*}
    fullpath=`ksh -c "(cd "${destdir}" 2>/dev/null && /bin/pwd)"`
	output="${fullpath}/${output}.mp4"	
	ffmpegpath=`dirname $0`	

	case "${twopass}" in
	0) 	#1pass
		$ffmpegpath/ffmpeg $over -i "${line}" -acodec aac -ab 90 -ac 2 -s 320x240 -vcodec h264 -b 768k -flags +loop -cmp +chroma -partitions +parti4x4+partp4x4+partp8x8+partb8x8 -flags2 +mixed_refs -me umh -subq 7 -trellis 2 -refs 5 -coder 0 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -bt 768k -maxrate 768k -bufsize 2M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 13 -f mp4 "${output}"  2>> /dev/console < /dev/null
		errorFF=$?
	;;
	1) #2pass
		$ffmpegpath/ffmpeg -y -i "${line}" -passlogfile /tmp/ffmpeg_automator -an -pass 1 -s 320x240 -vcodec h264 -b 768k -flags +loop -cmp +chroma -partitions 0 -me epzs -subq 1 -trellis 0 -refs 1 -coder 0 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -bt 768k -maxrate 768k -bufsize 2M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 13 -f mp4 /dev/null 2>> /dev/console	< /dev/null
		errorFF=$?
		$ffmpegpath/ffmpeg $over -i "${line}" -passlogfile /tmp/ffmpeg_automator -acodec aac -ab 90 -ar 48000 -ac 2 -pass 2 -s 320x240 -vcodec h264 -b 768k -flags +loop -cmp +chroma -partitions +parti4x4+partp4x4+partp8x8+partb8x8 -flags2 +mixed_refs -me umh -subq 7 -trellis 2 -refs 5 -coder 0 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -bt 768k -maxrate 768k -bufsize 2M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 13 -f mp4 "${output}"  2>> /dev/console < /dev/null
		errorFF=$?
	    if [ -r x264_2pass.log ]; then
			rm x264_2pass.log;
		fi
		if [ -r /tmp/ffmpeg_automator ]; then
			rm /tmp/ffmpeg_automator;
		fi
		;;
	esac	
	if [ $errorFF -eq 0 ]; then
		echo "$output";
	fi
	
done